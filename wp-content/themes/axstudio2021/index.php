<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AX_studio
 */

get_header();
?>

    <main class="main">

        <section>
            <div class="height-100 bg bg-fixed" style="background: url('<?php echo get_template_directory_uri(); ?>/screenshot.png'); background-position: center; background-size: cover;">

            </div>
        </section>

        <?php
        while (have_posts()) :
            the_post();

//            the_content();

        endwhile;
        ?>
    </main>

<?php
get_footer();
