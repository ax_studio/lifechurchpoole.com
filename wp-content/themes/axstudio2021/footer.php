<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>

</div>

<footer class="footer">
    <!--    <div id="footer-navigation" class="footer-navigation">-->
    <!--        --><?php
    //        wp_nav_menu(array(
    //            'theme_location' => 'footer',
    //            'menu_id' => 'footer-menu',
    //        ));
    //        ?>
    <!--    </div>-->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 footer_section">
                <img class="footer_logo" src="<?php echo site_url(); ?>/wp-content/uploads/2021/01/Life-Church-Poole.png" />
                <p>Poole : Central : BH15</p>
                <div class="footer_socialmedia">
                    <a title="Facebook"
                       href="https://www.facebook.com/pages/Poole-United-Kingdom/Word-of-Life-Church/30706442446"
                       target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a title="Twitter" href="https://twitter.com/lifechurchpoole" target="_blank"><i
                                class="fab fa-twitter"></i></a>
                    <a title="Instagram" href="https://instagram.com/lifechurchpoole" target="_blank"><i
                                class="fab fa-instagram"></i></a>
                    <a title="Youtube" href="https://www.youtube.com/c/LifeChurchPoole" target="_blank"><i
                                class="fab fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-md-2 col-12 footer_menu footer_section">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-left',
                    'menu_id' => 'footer-left',
                ));
                ?>

            </div>
            <div class="col-md-2 col-12 footer_menu footer_section">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-center',
                    'menu_id' => 'footer-center',
                ));
                ?>
            </div>
            <div class="col-md-2 col-12 footer_menu footer_section">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-right',
                    'menu_id' => 'footer-right',
                ));
                ?>
            </div>
            <div class="col-12 footer_section">
                <div class="footer-copyright">
                    <p class="small">Website By <a href="https://axstudio.uk" target="_blank">AX Studio</a>
                    </p>
                </div>
            </div>

        </div>
    </div>


</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
