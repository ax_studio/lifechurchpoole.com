<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="site">

    <header id="header" class="header">
        <div class="header-branding">
            <?php the_custom_logo(); ?>
            <a class="header-branding_white">
                <img src="<?php echo site_url(); ?>/wp-content/uploads/2021/01/lifechurchpoole_logo_white.png"/>
            </a>
        </div>

        <div id="header-navigation" class="header-navigation">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'primary',
                'menu_id' => 'header-menu',
            ));
            ?>

        </div>
        <div class="header-toggle">

            <div class="menu-wrapper"><span class="menu-cross"></span></div>
        </div>
    </header>

    <div class="site-content">
