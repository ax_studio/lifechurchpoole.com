<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package AX_studio
 */

get_header();
?>

    <main class="single">
        <?php
        while (have_posts()) :
            the_post();

            the_content();

        endwhile;
        ?>
    </main>

<?php
get_footer();
