gsap.registerPlugin(ScrollTrigger);




// Home Logo animation
gsap.utils.toArray(".logo_home").forEach(homeLogo => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: homeLogo,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(homeLogo, {
            scale: 0.9,
            opacity: 0,
        },
        {
            duration: 1.2,
            scale: 1,
            opacity: 1,
            delay: 0.3
        });
});

// Heading animation
gsap.utils.toArray("h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6").forEach(heading => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: heading,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(heading, {
            scale: 0.9,
            opacity: 0,
        },
        {
            duration: 0.7,
            scale: 1,
            opacity: 1,
            delay: 0.3
        });
});


// Paragraph animation
gsap.utils.toArray("p, .elementor-text-editor").forEach(paragraph => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: paragraph,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(paragraph, {
            opacity: 0
        },
        {
            opacity: 1,
            duration: 1.2,
            delay: 0.3

        });
});



// // Form animation
// gsap.utils.toArray("form").forEach(form => {
//     var tl = gsap.timeline({
//         scrollTrigger: {
//             trigger: form,
//             toggleActions: "restart pause pause reset",
//             start: "top 100%"
//         }
//     });
//
//     tl.fromTo(form, {
//             opacity: 0,
//         },
//         {
//             opacity: 1,
//             duration: 0.7,
//             delay: 0.3
//         });
// });

// Buttons animation
gsap.utils.toArray("input[type=submit], .button, button, .elementor-button").forEach(buttons => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: buttons,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(buttons, {
            opacity: 0,
            y: 30
        },
        {
            opacity: 1,
            y: 0,
            duration: 0.7
        });
});


// Youtube animation
gsap.utils.toArray("iframe").forEach(youtubeContainer => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: youtubeContainer,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(youtubeContainer, {
            opacity: 0,
            scale: 0.9
        },
        {
            opacity: 1,
            scale: 1,
            duration: 0.7,
            delay: 0.3
        });
});





// Footer animation
gsap.utils.toArray(".footer_section").forEach(footerSection => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: footerSection,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(footerSection, {
            opacity: 0,
            scale: 0.9
        },
        {
            scale: 1,
            opacity: 1,
            duration: 0.7
        });
});


// Header animation
gsap.utils.toArray(".header > div").forEach(headerSection => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: headerSection,
            toggleActions: "restart none none none",
            start: "top 100%"
        }
    });

    tl.fromTo(headerSection, {
            opacity: 0,
            scale: 0.9
        },
        {
            scale: 1,
            opacity: 1,
            duration: 0.7
        });
});