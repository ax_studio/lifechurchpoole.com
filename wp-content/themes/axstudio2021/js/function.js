function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function onScroll() {
    $(window).scroll(function () {

        if ($(this).scrollTop() > 70) {
        } else {
        }
    });
}

function menuToggle() {
    $('.menu-wrapper').click(function() {
        $(this).toggleClass('active');

        if ($(this).hasClass('active')) {
            $('.header-navigation').fadeIn();
            $('.header-branding, .header-navigation').addClass('active');
        } else {
            $('.header-navigation').fadeOut();
            $('.header-branding, .header-navigation').removeClass('active');
        }
    });


}


function animateWhenScroll(){
    // $(window).scroll( function(){
    //     $('.elementor-section').each( function(i){
    //         var bottom_of_object = $(this).offset().top + 0;
    //         var bottom_of_window = $(window).scrollTop() + $(window).height();
    //         if( bottom_of_window > bottom_of_object ){
    //             $(this).addClass('animate_show');
    //         }
    //         if( bottom_of_window < bottom_of_object ){
    //             $(this).removeClass('animate_show');
    //         }
    //     });
    // });
}