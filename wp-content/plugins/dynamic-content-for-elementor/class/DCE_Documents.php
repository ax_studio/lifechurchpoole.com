<?php

namespace DynamicContentForElementor;

/**
 * Documents Class
 *
 * Register new elementor widget.
 *
 * @since 0.0.1
 */
class DCE_Documents {

	public $documents = [];
	public static $registered_documents = [];
	public static $dir = DCE_PATH . 'includes/documents';
	public static $namespace = '\\DynamicContentForElementor\\Documents\\';

	public function __construct() {
		$this->init();
	}

	public function init() {
		$this->documents = self::get_documents();
	}

	public static function get_documents() {
		$tmpDocuments = [];
		$documents = glob(self::$dir . '/DCE_*.php');
		foreach ( $documents as $key => $value ) {
			$class = pathinfo($value, PATHINFO_FILENAME);
			if ( $class != 'DCE_Document_Prototype' ) {
				$tmpDocuments[ strtolower($class) ] = $class;
			}
		}
		return $tmpDocuments;
	}

	public static function get_active_documents() {
		$tmpDocuments = self::get_documents();
		self::includes();
		$activeDocuments = array();
		foreach ( $tmpDocuments as $key => $name ) {
			$class = self::$namespace . $name;
			if ( $class::is_enabled() ) {
				$activeDocuments[ $key ] = $name;
			}
		}
		return $activeDocuments;
	}

	/**
	 * On extensions Registered
	 *
	 * @since 0.0.1
	 *
	 * @access public
	 */
	public function on_documents_registered() {
		$this->includes();
		$this->register_documents();
	}

	public static function includes() {
		require_once self::$dir . '/DCE_Document_Prototype.php'; // obbligatorio in quanto esteso dagli altri

		foreach ( self::get_documents() as $key => $value ) {
			require_once self::$dir . '/' . $value . '.php';
		}
	}

	/**
	 * On Controls Registered
	 *
	 * @since 1.0.4
	 *
	 * @access public
	 */
	public function register_documents() {
		if ( empty(self::$registered_documents) ) {
			$excluded_documents = json_decode(get_option(SL_PRODUCT_ID . '_excluded_documents', '[]'), true);
			foreach ( $this->documents as $key => $name ) {
				if ( ! isset($excluded_documents[ $name ]) ) { // controllo se lo avevo escluso in quanto non interessante
					$class = self::$namespace . $name;
					if ( $class::is_enabled() ) {
						$document = new $class();
						self::$registered_documents[ $name ] = $document;
						DCE_Assets::add_depends($document);
					}
				}
			}
		}
	}

}
