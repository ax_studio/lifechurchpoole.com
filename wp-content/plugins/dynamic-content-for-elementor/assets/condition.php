<?php
// safe check for visibility condition

/** Loads the WordPress Environment and Template */
define('WP_USE_THEMES', false);
require '../../../../wp-blog-header.php';

$element_id = $_GET['eid'];
$post_id = intval($_GET['pid']);

global $post, $wp_query;
$post = get_post($post_id);
if ( $post ) {
	setup_postdata($post);
	$wp_query->queried_object = $post;
	$wp_query->queried_object_id = $post_id;
}

if ( $element_id && $post_id ) {

	$php_code = get_post_meta($post_id);
	$settings = \DynamicContentForElementor\DCE_Helper::get_elementor_element_settings_by_id($element_id, $post_id);

	if ( isset($settings['dce_visibility_custom_condition_php']) ) {
		$php_code = $settings['dce_visibility_custom_condition_php'];
		if ( $php_code ) {
			if ( strpos($php_code, 'return ') !== false ) {
				$return = eval($php_code);
				if ( $return ) {
					echo '1';
					die();
				}
			}
		}
	}
}

echo '0';
