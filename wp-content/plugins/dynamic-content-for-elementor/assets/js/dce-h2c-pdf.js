let el = document.querySelector(window.pdfSelector);
// Remove responsive images attributes, otherwise html2canvas can crop results
// after changing viewport.
let imgs = el.querySelectorAll('img');
imgs.forEach((img) => {
	img.removeAttribute("srcset");
	img.removeAttribute("sizes");
})
// Hide the pdf button:
let pdfButton = el.getElementsByClassName("elementor-button-pdf-wrapper");
if (pdfButton.length > 0) {
		var pdfButtonDisplay = pdfButton[0].style.display;
		pdfButton[0].style.display = 'none';
}
function downloadPDF() {
	if (! el) {
		alert("Could not find the selected element in the page.");
		return;
	}
	pdfBeforeRendering();
	const doc = new jspdf.jsPDF({
		format: window.pdfPageSize, 
		orientation: window.pdfOrientation,
		unit: window.pdfMarginUnit
	});
	const pdfWidth = doc.internal.pageSize.getWidth();
	const pdfHeight = doc.internal.pageSize.getHeight();
	const pdfElWidth = pdfWidth - window.pdfMarginLeft - window.pdfMarginRight;
	const pdfElHeight = pdfHeight - window.pdfMarginTop - window.pdfMarginBottom;
	html2canvas(el, { windowWidth: 1024, windowHeight: 768 }).then(canvas => {
		const imgData = canvas.toDataURL('image/png');              
		// The following is necesary for the case where canvas is so long that
		// it doesn't fit in one page.
		// https://stackoverflow.com/questions/24069124/how-to-save-a-image-in-multiple-pages-of-pdf-using-jspdf
		// Get canvas height that can be filled in a page, in pixels.
		const pageHeight = pdfElHeight * canvas.width / pdfElWidth;
		for (let hposition = 0; hposition < canvas.height; hposition+=pageHeight) {
			// The final slice height could be less than the pageHeight.
			const sliceHeight = Math.min(canvas.height - hposition, pageHeight);
			const slicePdfHeight = (sliceHeight * pdfElHeight) / pageHeight;
			// In order to get the image slice we have to do the drawing on another canvas.
			const onePageCanvas = document.createElement("canvas");
			onePageCanvas.setAttribute('width', canvas.width);
			onePageCanvas.setAttribute('height', sliceHeight);
			const ctx = onePageCanvas.getContext('2d');
			ctx.drawImage(canvas, 0, hposition, canvas.width, sliceHeight, 0, 0, 
				canvas.width, sliceHeight);
			const sliceData = onePageCanvas.toDataURL("image/png");
			// At the beginning of the loop we already have a page.
			if (hposition != 0) {
				doc.addPage();
			}
			doc.addImage( sliceData, '', window.pdfMarginLeft,
				window.pdfMarginTop, pdfElWidth, slicePdfHeight);
		}
		doc.save(window.pdfTitle);
	});
}
downloadPDF();
if (pdfButton.length > 0) {
		pdfButton[0].style.display = pdfButtonDisplay;
}
pdfAfterRendering();
