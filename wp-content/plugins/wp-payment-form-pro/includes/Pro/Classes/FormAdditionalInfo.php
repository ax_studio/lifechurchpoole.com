<?php

namespace WPPayForm\Pro\Classes;

use WPPayForm\Classes\ArrayHelper;
use WPPayForm\Classes\Models\Forms;
use WPPayForm\Classes\Models\Submission;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Form Info Handler Class
 * @since 1.0.0
 */
class FormAdditionalInfo
{
    public function register()
    {
        add_shortcode('payform_info', array($this, 'formInfoHandler'));
    }

    public function formInfoHandler($args)
    {
        $args = shortcode_atts(array(
            'id'             => '',
            'info'           => '',
            'payment_status' => ''
        ), $args);
        extract($args);
        $form = Forms::getForm($id);
        if (!$id || !$info || !$form) {
            return;
        }
        $validInfos = array(
            'submission_left'  => 'getSubmissionLeftCount',
            'submission_total' => 'getSubmissionTotal'
        );
        if (isset($validInfos[$info])) {
            return $this->{$validInfos[$info]}($form, $args);
        }
        return '';
    }

    public function getSubmissionLeftCount($form, $args)
    {
        if (!get_post_meta($form->ID, 'wppayform_form_scheduling_settings', true)) {
            return '';
        }
        $sheduleSettings = Forms::getSchedulingSettings($form->ID);
        if (ArrayHelper::get($sheduleSettings, 'limitNumberOfEntries.status') == 'yes') {
            $limitEntrySettings = ArrayHelper::get($sheduleSettings, 'limitNumberOfEntries');
            $limitPeriod = ArrayHelper::get($limitEntrySettings, 'limit_type');
            $numberOfEntries = ArrayHelper::get($limitEntrySettings, 'number_of_entries');
            $paymentStatuses = ArrayHelper::get($limitEntrySettings, 'limit_payment_statuses');
            $submissionModel = new Submission();
            $totalEntryCount = $submissionModel->getEntryCountByPaymentStatus($form->ID, $paymentStatuses, $limitPeriod);
            if ($totalEntryCount >= intval($numberOfEntries)) {
                return '0';
            }
            return $numberOfEntries - $totalEntryCount;
        }
        return '';
    }

    public function getSubmissionTotal($form, $args)
    {
        $statuses = ArrayHelper::get($args, 'payment_status');
        if ($statuses) {
            $statuses = explode(',', $statuses);
        }
        $submissionModel = new Submission();
        return $submissionModel->getEntryCountByPaymentStatus($form->ID, $statuses, 'total');
    }
}