<?php return array(
    'root' => array(
        'name' => 'wpfluent/wpfluent',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '803617598d51fd88c44774ce6e4ae2950936fc4b',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'wpfluent/framework' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '7359c1eba0b440896cef1aa2d6ce89767c5cd96f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wpfluent/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wpfluent/wpfluent' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '803617598d51fd88c44774ce6e4ae2950936fc4b',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
