<?php

namespace WPPayForm\App\Services;

if (!defined('ABSPATH')) {
    exit;
}


class ProRoutes 
{

    public static function getRoutes()
    {
        $default = array(
            [
                'path' => 'stripe',
                'name' => 'stripe',
                'meta'=> [
                    'title' => 'Stripe' 
                ]
            ],
            [
                'path' => 'paypal',
                'name' => 'paypal',
                'meta'=> [
                    'title' => 'PayPal' 
                ]
            ]
        );
        
        $premium = array(
            [
                'path' => 'mollie',
                'name' => 'mollie',
            ],
            [
                'path' => 'razorpay',
                'name' => 'razorpay',
            ],
            [
                'path' => 'paystack',
                'name' => 'paystack',
            ],
            [
                'path' => 'square',
                'name' => 'square',
            ],
            [
                'path' => 'payrexx',
                'name' => 'payrexx',
            ],
            [
                'path' => 'billplz',
                'name' => 'billplz',
            ],
            [
                'path' => 'sslcommerz',
                'name' => 'sslcommerz',
            ],
            [
                'path' => 'offline',
                'name' => 'offline',
            ],
        );

        return defined('WPPAYFORMHASPRO') ? $default : array_merge($default, $premium);
    }

    public static function getMethods()
    {
        $default =  array(
            [
                'title' => 'Stripe',
                'route_name' => 'stripe',
                'svg' => 'stripe.svg',
            ],
            [
                'title' => 'PayPal',
                'route_name' => 'paypal',
                'svg' => 'paypal.svg',
                'route_query' => [],
            ]
        );

        $premium = array(
            [
                'title' => 'Mollie',
                'route_name' => 'mollie',
                'svg' => 'mollie.svg',
                'route_query' => [],
            ],
            [
                'title' => 'Razorpay',
                'route_name' => 'razorpay',
                'route_query' => [],
                'svg' => 'razorpay.svg',
            ],
            [
                'title' => 'Paystack',
                'route_name' => 'paystack',
                'route_query' => [],
                'svg' => 'paystack.svg',
            ],
            [
                'title' => 'Square',
                'route_name' => 'square',
                'route_query' => [],
                'svg' => 'square.svg',
            ],
            [
                'title' => 'Payrexx',
                'route_name' => 'payrexx',
                'route_query' => [],
                'svg' => 'payrexx.svg',
            ],
            [
                'title' => 'Billplz',
                'route_name' => 'billplz',
                'route_query' => [],
                'svg' => 'billplz.svg',
            ],
            [
                'title' => 'SSLCommerz',
                'route_name' => 'sslcommerz',
                'route_query' => [],
                'svg' => 'sslcommerz.svg',
            ],
            [
                'title' => 'Offline',
                'route_name' => 'offline',
                'route_query' => [],
                'svg' => 'offline.svg',
            ],
        );

        return defined('WPPAYFORMHASPRO') ? $default : array_merge($default, $premium);
    }

}