<?php
/**
 * @ Author: Bill Minozzi
 * @ Copyright: 2020 www.BillMinozzi.com
 * @ Modified time: 2021-03-02 12:42:23
 */
if (!defined('ABSPATH')) {
    die('We\'re sorry, but you can not directly access this file.');
} 
global $wpmemory_memory;
global $wpmemory_checkversion;
//display form
echo '<div class="wrap-wpmemory ">' . "\n";
echo '<h2 class="title">WordPress Memory Limit</h2>' . "\n";
echo '<p class="description">';
esc_attr_e("WordPress Memory Limit is the maximum amount of memory that can be consumed by PHP.","wp-memory");
echo '<br />';
esc_attr_e("WP_MEMORY_LIMIT option allows you to specify that in your wp-config.php file on your root folder.","wp-memory");
echo "</p>" . "\n";
$mb = 'MB';
echo '<hr>';
esc_attr_e("Total Current WordPress Memory Limit:","wp-memory");
echo ' ' . esc_attr($wpmemory_memory['wp_limit']) . esc_attr($mb);
echo '<hr>';
echo '<br />';
echo '<a href="http://wpmemory.com/fix-low-memory-limit/">';
esc_attr_e("Click Here to Learn More","wp-memory");
echo '</a>';
echo '<br />';
echo '<br />';
esc_attr_e("This Amount need be minor than PHP Memory Limit.","wp-memory");
echo '<br />';
esc_attr_e("We can update it for you. Just select below and click FIX IT NOW.","wp-memory");
echo ' ';
esc_attr_e("(We will make one backup and update the file wp-config.php)","wp-memory");
echo '<br />';
if (empty($wpmemory_checkversion)) {
    echo '<b>';
    esc_attr_e("Free Version max memory upgrade is 64M","wp-memory");
    echo '<br />';
    esc_attr_e("Go Premium and you can setup up the limit up to 1024M. Just click the premium Tab above.","wp-memory").'</b>';
}
echo '<form class="wpmemory -form" method="post" action="admin.php?page=wp_memory_admin_page&tab=wpmemory">' . "\n";
echo '<input type="hidden" name="process" value="wp_memory_admin_page"/>' . "\n";
echo '<br />' . "\n";
//echo '</form>' . "\n";
$wpmeml = $wpmemory_memory['wp_limit'];
?>
<label for="wpmemorylimit"><?php esc_attr_e("Update the WordPress Memory Limit to","wp-memory");?>:</label>
<select name="wp_memory_select" id="wp_memory_select">
    <option value="40" <?php echo (esc_attr($wpmeml) == '40') ? ' selected="selected"' : ''; ?>>40 MB</option>
    <option value="64" <?php echo (esc_attr($wpmeml) == '64') ? ' selected="selected"' : ''; ?>>64 MB</option>
   
   <?php

    if (!empty($wpmemory_checkversion)) { ?>
    <option value="96" <?php echo (esc_attr($wpmeml) == '96') ? ' selected="selected"' : ''; ?>>96 MB</option>
    <option value="128" <?php echo (esc_attr($wpmeml) == '128') ? ' selected="selected"' : ''; ?>>128 MB</option>
    <option value="256" <?php echo (esc_attr($wpmeml) == '256') ? ' selected="selected"' : ''; ?>>256 MB</option>
    <option value="512" <?php echo (esc_attr($wpmeml) == '512') ? ' selected="selected"' : ''; ?>>512 MB</option>
    <option value="1024" <?php echo (esc_attr($wpmeml) == '1024') ? ' selected="selected"' : ''; ?>>1024 MB</option>
   <?php } 
    else { ?>
        <option value="64" <?php echo (esc_attr($wpmeml) == '64') ? ' selected="selected"' : ''; ?>>96 MB   (only Premium)</option>
        <option value="64" <?php echo (esc_attr($wpmeml) == '64') ? ' selected="selected"' : ''; ?>>128 MB  (only Premium)</option>
        <option value="64" <?php echo (esc_attr($wpmeml) == '64') ? ' selected="selected"' : ''; ?>>256 MB  (only Premium)</option>
        <option value="64" <?php echo (esc_attr($wpmeml) == '64') ? ' selected="selected"' : ''; ?>>512 MB  (only Premium)</option>
        <option value="64" <?php echo (esc_attr($wpmeml) == '64') ? ' selected="selected"' : ''; ?>>1024 MB (only Premium)</option>
    <?php } ?>


</select>
<br />
<?php

echo '<br />';
echo '<img src="'.esc_url(WPMEMORYIMAGES).'/attention.gif" width="70">';
echo '<p style="color:red; margin-top: -10px;">';
esc_attr_e("Next window, before proceed, copy the link location to restore your wp-config.php","wp-memory");
echo '<br />';
esc_attr_e('Some hosting company, to "protect you", can damage the file wp-config.php.',"wp-memory");
echo '<br />';
esc_attr_e('With the link, you can restore it in a few seconds.',"wp-memory");
echo '<br />';
esc_attr_e('Another good idea is make a download of your wp-config.php file. (located at your root folder)',"wp-memory");
echo '</p>';
//echo '<br />';

// echo '<br />';

echo '<a href="#" id="themefix-wpconfig-button" class="button button-primary">Fix it Now!</a>';
echo '</form>' . "\n";
echo '<div class="main-notice">';
echo '</div>' . "\n";
echo '</div>';
// usar outra coisa?
// colocar um cookie?


$verticalmenu_urlkey = urlencode(substr(NONCE_KEY, 0, 10));
$verticalmenu_mypath = WPMEMORYURL . 'dashboard/fixconfig/fixconfig.php';
//$verticalmenu_myrestore = WPMEMORYURL . 'public/restore-config.php?key=' . $verticalmenu_urlkey;
$verticalmenu_myrestore = WPMEMORYURL . 'public/restore-config.php';
$verticalmenu_email = get_bloginfo('admin_email');
global $wpmemory_memory;
?>
<!-- ///////////// Fix Config /////////////////  -->
<div id="themefix-wpconfig" style="display: none;">
    <div class="themefix-message-wrap" style="">
        <div class="themefix-message" style="">
            <?php esc_attr_e("If your server allow us, we can try to fix your file wp-config.php to release more memory.","wp-memory");?>
            <br />
            <img src="<?php echo esc_url(WPMEMORYIMAGES); ?>/attention.gif" width="50">
            <p style="color:red; margin-top: -10px;">
            <strong><?php esc_attr_e("Please, copy the url blue below to safe place before to proceed.","wp-memory");?></strong>
            </p>
            <?php esc_attr_e("Use the url only to undo this operation if you've problem accessing your site after the update.","wp-memory");?>
            <br />
            <br />
            <?php esc_attr_e("After Copy the URL, click UPDATE to proceed or Cancel to abort.","wp-memory");?>
            <br /> <br />
            <textarea rows="3" id="restore_wpconfig" name="restore_wpconfig" style="width:100%; color: blue;"><?php echo esc_attr($verticalmenu_myrestore); ?></textarea>
            <textarea rows="6" id="feedback_wpconfig" name="feedback_wpconfig" style="width:100%; font-weight: bold;"></textarea>
            <br /><br />
            <img alt="aux" src="/wp-admin/images/wpspin_light-2x.gif" id="wpmemory-imagewait20" />
            <input type="hidden" id="email" name="email" value="<?php echo esc_attr($verticalmenu_email); ?>" />
            <input type="hidden" id="url_config" name="url_config" value="<?php echo esc_attr($verticalmenu_mypath); ?>" />
            <input type="hidden" id="verticalmenu_urlkey" name="server_memory" value="<?php echo esc_attr($verticalmenu_urlkey); ?>" />
            <input type="hidden" id="server_memory" name="server_memory" value="<?php echo (int) esc_attr(ini_get('memory_limit')) ?>" />
            <a href="#" id="button-close-wpconfig" class="button button-primary button-close-wpconfig"><?php esc_attr_e("Update", "wp-memory"); ?></a>
            <a href="#" id="button-cancell-wpconfig" class="button button-primary button-cancell-wpconfig"><?php esc_attr_e("Cancel", "wp-memory"); ?></a>
            <br /><br />
        </div>
    </div>
</div>
<!-- ///////////// End Fix config /////////////////  -->