=== Memory Usage, Memory Limit, PHP and Server Memory Health Check and Fix Plugin ===
Contributors: sminozzi
Tags: memory limit, memory usage, memory exhausted, fix memory, wpmemory, current memory usage, memory requirements
Requires at least: 5.2
Tested up to: 6.1
Stable tag: 2.53
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WPmemory Check High Memory Usage, Memory Limit, PHP Memory, hardware memory, errors & show result in Site Health Page & fix low memory limit.

== Description ==
**WP MEMORY CHECK AND FIX**
★★★★★<br>

>Plugin to Check For High Memory Usage (current memory usage), WordPress Memory Limit  and include result in the Site Health Page and Give Suggestions how to improve it. Allows you to increase the Server Php Memory ("Memory Limit") without editing any file. The plugin can show you also the total server RAM (Random Access Memory), your physical memory installed.
>Multilingual ready: English, Italian, Portuguese and Spanish files included. 
.
>Português: Plugin para verificar e corrigir se há alto uso de memória. Todo o interface pode ser visto em Português.
.
>Italiano: Plugin per verificare e aggiustare l'utilizzo elevato della memoria. L'intera interfaccia può essere visualizzata in Italiano.
.
>Español: Plugin para verificar y arreglar el alto uso de memoria. Toda la interfaz se puede ver en Español.

WordPress memory requirements: Never get "Fatal error: Allowed memory size of xxx bytes exhausted again".

Very easy to install and manage. Just go to your plugin page to activate / deactivate. 

Go to your Site  Health  Page (Dashboard => Tools => Site Health - Status) and look for Memory Usage 

Go to Dashboard => Tools => WP Memory for dashboard and help page with tips and suggestions.

For Help You with Debug, look also the Info Page  (Dashboard => Tools => Site Health - Info).   Look for Memory Usage 

Show the WordPress errors (PHP errors error_log, error log or error reporting). Fatal Errors, Warnings, Alerts and Parse or syntax errors to Help you to discovery and fix issues.

You need WordPress 5.2 or higher to install this plugin.

>Attention
>If you request to our plugin make changes on your wp-config.php, the plugin will send you by email the Emergency Restore Link. 
>Please keep that window opened until testing the site in another window. You can also copy the Emergency Restore link address (copy and paste) to DISCARD last changes if necessary. 
>Some hosting company, to "protect you", can make changes and destroy your wp-config.php file...

You can find the:
<ul>
<li><a href="http://wpmemory.com/" target="_self">Plugin Site</a></li>
<li><a href="http://wpmemory.com/help/index.php" target="_self">Online Documentation</a></li>
<li><a href="https://wordpress.org/plugins/wptools/">WPtools - More than 35 Useful free WordPress Tools</a></li>
<li><a href="http://wpmemory.com/share/">Share</a> :) </li>
</ul> 



== Installation ==


1) Install via wordpress.org

2) Activate the plugin through the 'Plugins' menu in WordPress

or

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.


== Frequently Asked Questions ==

How to Install?

1) Install via wordpress.org

2) Activate the plugin through the 'Plugins' menu in WordPress

or

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.

3) The Site doesn't work after change the wp memory limit.
Nothing is lost or deleted. You can easily get your site back.
Check you email for our email with recovery link.
If that fail, you can recover the backup connecting to your site via FTP.
Locate the backup of the old, working wp-config.php file in /wp-content/plugins/wp-memory/public/ and copy to your site’s root folder and you’re back in business.


4) Where can I get more free info about memory?

Visit the <a href="http://wpmemory.com/" target="_self">Plugin Site</a>

== Screenshots ==
1. Memory Usage (Memory Limit, PHP Memory, hardware memory)
2. Page Health Info
3. Site-Health: Memory
4. Server Errors
5. Wizard Step 1
6. Wizard Step 2
7. Wizard Step 3

== Español ==
Este plugin comprueba el alto uso de memoria e incluye el resultado en la página Herramientas => Salud del sitio.Este plugin también comprueba el estado de la memoria y le permite aumentar el límite de memoria de Php (Php Memory Limity) el límite de memoria de WordPress (WordPress Memory Limit ) sin editar ningún archivo.

== Português ==
Este plugin verifica a utilização da memória e no caso de muita utilização, inclui o resultado em Ferramentas => Saúde do Site.Este plugin também verifica o status da memória e permite aumentar o limite de memória PHP e o limite de memória WordPress (WordPress Memory Limit) sem editar nenhum arquivo.

== Italiano ==
Questo plugin controlla l'uso di memoria e include il risultato nella pagina Strumenti => Salute del sito.Questo plugin controlla anche lo stato della memoria e permette di aumentare il Php Memory Limit e WordPress Memory Limit senza modificare alcun file.

== Look the file changelog.txt for details ==
We can keep the size of the standard WordPress readme.txt file reasonable.

== Tags ==
Out of memory
memory allocation
stopping site because it exceeded memory limits
check site memory usage
how to see your Gb ram
memory website hosting
memory increase
memory error limit exceeded
ram
