<?php
/**
 * @ Author: Bill Minozzi
 * @ Copyright: 2020 www.BillMinozzi.com
 * @ Modified time: 2021-03-03 08:58:41
 */
if (!defined('ABSPATH')) {
    die('We\'re sorry, but you can not directly access this file.');
}
$wpmemory_now = strtotime("now");
$wpmemory_after = strtotime("now") + (3600);
function wpmemory_enqueue_scripts()
{
    wp_register_script('wpmemory-fix-config-manager', WPMEMORYURL . 'dashboard/fixconfig/wp-memory-fix-config-manager.js', array('jquery'), WPMEMORYVERSION, true);
    wp_enqueue_script('wpmemory-fix-config-manager');
    wp_enqueue_style('bill-help-wpmemory', WPMEMORYURL . 'dashboard/fixconfig/help.css');
}
add_action('admin_init', 'wpmemory_enqueue_scripts');
if (!function_exists('ini_get')) {
	function wpmemory_general_admin_notice2()
	{
		if (is_admin()) {
			echo '<div class="notice notice-warning is-dismissible">
				 <p>Your server doesn\'t have a PHP function ini_get.</p>
				 <p>Please, talk with your hosting company.</p>
			 </div>';
		}
	}
	add_action('admin_notices', 'wpmemory_general_admin_notice');
}
function wpmemory_memory_test()
{
    global $wpmemory_memory, $wpmemory_usage_content, $wpmemory_label, $wpmemory_status, $wpmemory_description, $wpmemory_actions;
    $result = array(
        'badge' => array(
            'label' => $wpmemory_label,
            'color' => $wpmemory_memory['color'],
        ),
        'test' => 'wpmemory_test',
        // status: Section the result should be displayed in. Possible values are good, recommended, or critical.
        'status' => $wpmemory_status,
        'label' => __('Memory Usage', 'wp-memory' ),
        'description' => $wpmemory_description . '  ' . $wpmemory_usage_content,
        'actions' => $wpmemory_actions
    );
    return $result;
}
function wpmemory_add_debug_info($debug_info)
{
    global $wpmemory_usage_content;
    $debug_info['wpmemory'] = array(
        'label' => __('Memory Usage', 'wp-memory' ),
        'fields' => array(
            'memory' => array(
                'label' => __('Memory Usage information', 'wp-memory' ),
                'value' => strip_tags($wpmemory_usage_content),
                'private' => true
            )
        )
    );
    return $debug_info;
}
function wpmemory_activation()
{
    global $wp_version;
    if (version_compare(PHP_VERSION, '5.3', '<')) {
        deactivate_plugins(plugin_basename(__FILE__));
        load_plugin_textdomain('wpmemory', false, dirname(plugin_basename(__FILE__)) . '/language/');
        $plugin_data    = get_plugin_data(__FILE__);
        $plugin_version = $plugin_data['Version'];
        $plugin_name    = $plugin_data['Name'];
        wp_die('<h1>' . __('Could not activate plugin: PHP version error', 'wp-memory' ) . '</h1><h2>PLUGIN: <i>' . $plugin_name . ' ' . $plugin_version . '</i></h2><p><strong>' . __('You are using PHP version', 'wp-memory' ) . ' ' . PHP_VERSION . '</strong>. ' . __('This plugin has been tested with PHP versions 5.3 and greater.', 'wp-memory' ) . '</p><p>' . __('WordPress itself <a href="https://wordpress.org/about/requirements/" target="_blank">recommends using PHP version 7 or greater</a>. Please upgrade your PHP version or contact your Server administrator.', 'wp-memory' ) . '</p>', __('Could not activate plugin: PHP version error', 'wp-memory' ), array(
            'back_link' => true
        ));
    }
    if (version_compare($wp_version, '5.2') < 0) {
        deactivate_plugins(plugin_basename(__FILE__));
        load_plugin_textdomain('wpmemory', false, dirname(plugin_basename(__FILE__)) . '/language/');
        $plugin_data    = get_plugin_data(__FILE__);
        $plugin_version = $plugin_data['Version'];
        $plugin_name    = $plugin_data['Name'];
        wp_die('<h1>' . __('Could not activate plugin: WordPress need be 5.2 or bigger.', 'wp-memory' ) . '</h1><h2>PLUGIN: <i>' . $plugin_name . ' ' . $plugin_version . '</i></h2><p><strong>' . __('Please, Update WordPress to Version 5.2 or bigger to use this plugin.', 'wp-memory' ) . '</strong>', array(
            'back_link' => true
        ));
    }
}
function wp_memory_activ_message()
{
    if (get_transient('wpmemory-admin-notice')) {
        echo '<div class="updated"><p>';
        $bd_msg = '<h2>';
        $bd_msg .= __('WP Memory  Plugin was activated!', "wp-memory");
        $bd_msg .= '</h2>';
        $bd_msg .= '<h3>';
        $bd_msg .= __('For details and help, take a look at WP Memory  at your left menu, Tools', "wp-memory");
        $bd_msg .= '<br />';
        $bd_msg .= ' <a class="button button-primary" href="admin.php?page=wp_memory_admin_page">';
        $bd_msg .= __('or click here', "wp-memory");
        $bd_msg .= '</a>';

		$allowed_atts = array(
			'align'      => array(),
			'class'      => array(),
			'type'       => array(),
			'id'         => array(),
			'dir'        => array(),
			'lang'       => array(),
			'style'      => array(),
			'xml:lang'   => array(),
			'src'        => array(),
			'alt'        => array(),
			'href'       => array(),
			'rel'        => array(),
			'rev'        => array(),
			'target'     => array(),
			'novalidate' => array(),
			'type'       => array(),
			'value'      => array(),
			'name'       => array(),
			'tabindex'   => array(),
			'action'     => array(),
			'method'     => array(),
			'for'        => array(),
			'width'      => array(),
			'height'     => array(),
			'data'       => array(),
			'title'      => array(),

			'checked' => array(),
			'selected' => array(),


		);

		$my_allowed['form'] = $allowed_atts;
		$my_allowed['select'] = $allowed_atts;
		// select options
		$my_allowed['option'] = $allowed_atts;
		$my_allowed['style'] = $allowed_atts;
		$my_allowed['label'] = $allowed_atts;
		$my_allowed['input'] = $allowed_atts;
		$my_allowed['textarea'] = $allowed_atts;

        //more...future...
		$my_allowed['form']     = $allowed_atts;
		$my_allowed['label']    = $allowed_atts;
		$my_allowed['input']    = $allowed_atts;
		$my_allowed['textarea'] = $allowed_atts;
		$my_allowed['iframe']   = $allowed_atts;
		$my_allowed['script']   = $allowed_atts;
		$my_allowed['style']    = $allowed_atts;
		$my_allowed['strong']   = $allowed_atts;
		$my_allowed['small']    = $allowed_atts;
		$my_allowed['table']    = $allowed_atts;
		$my_allowed['span']     = $allowed_atts;
		$my_allowed['abbr']     = $allowed_atts;
		$my_allowed['code']     = $allowed_atts;
		$my_allowed['pre']      = $allowed_atts;
		$my_allowed['div']      = $allowed_atts;
		$my_allowed['img']      = $allowed_atts;
		$my_allowed['h1']       = $allowed_atts;
		$my_allowed['h2']       = $allowed_atts;
		$my_allowed['h3']       = $allowed_atts;
		$my_allowed['h4']       = $allowed_atts;
		$my_allowed['h5']       = $allowed_atts;
		$my_allowed['h6']       = $allowed_atts;
		$my_allowed['ol']       = $allowed_atts;
		$my_allowed['ul']       = $allowed_atts;
		$my_allowed['li']       = $allowed_atts;
		$my_allowed['em']       = $allowed_atts;
		$my_allowed['hr']       = $allowed_atts;
		$my_allowed['br']       = $allowed_atts;
		$my_allowed['tr']       = $allowed_atts;
		$my_allowed['td']       = $allowed_atts;
		$my_allowed['p']        = $allowed_atts;
		$my_allowed['a']        = $allowed_atts;
		$my_allowed['b']        = $allowed_atts;
		$my_allowed['i']        = $allowed_atts;
     	

		echo wp_kses($bd_msg, $my_allowed);


       // echo $bd_msg;





        echo "</p></h3></div>";
        delete_transient('wpmemory-admin-notice');
    }
}
function wpmemory_admin_notice_activation_hook()
{
    /* Create transient data */
    set_transient('wpmemory-admin-notice', true, 5);
}
function wp_memory_init()
{
    add_management_page(
        'WP Memory',
        'WP Memory',
        'manage_options',
        'wp_memory_admin_page', // slug
        'wp_memory_admin_page'
    );
}
function wpmemory_check_memory()
{
    global $wpmemory_memory;
    $wpmemory_memory['limit'] = (int) ini_get('memory_limit');
    $wpmemory_memory['usage'] = function_exists('memory_get_usage') ? round(memory_get_usage() / 1024 / 1024, 0) : 0;
    if (!defined("WP_MEMORY_LIMIT")) {
        $wpmemory_memory['msg_type'] = 'notok';
        return;
    }
    $wpmemory_memory['wp_limit'] =  trim(WP_MEMORY_LIMIT);
    if ($wpmemory_memory['wp_limit'] > 9999999)
        $wpmemory_memory['wp_limit'] = ($wpmemory_memory['wp_limit'] / 1024) / 1024;
    if (!is_numeric($wpmemory_memory['usage'])) {
        $wpmemory_memory['msg_type'] = 'notok';
        return;
    }
    if (!is_numeric($wpmemory_memory['limit'])) {
        $wpmemory_memory['msg_type'] = 'notok';
        return;
    }
    
    if ($wpmemory_memory['limit'] > 9999999)
       $wpmemory_memory['limit'] = ($wpmemory_memory['limit'] / 1024) / 1024;	


    if ($wpmemory_memory['usage'] < 1) {
        $wpmemory_memory['msg_type'] = 'notok';
        return;
    }
    $wplimit = $wpmemory_memory['wp_limit'];
    $wplimit = substr($wplimit, 0, strlen($wplimit) - 1);
    $wpmemory_memory['wp_limit'] = $wplimit;
    $wpmemory_memory['percent'] = $wpmemory_memory['usage'] / $wpmemory_memory['wp_limit'];
    $wpmemory_memory['color'] = 'font-weight:normal;';
    if ($wpmemory_memory['percent'] > .7) $wpmemory_memory['color'] = 'font-weight:bold;color:#E66F00';
    if ($wpmemory_memory['percent'] > .85) $wpmemory_memory['color'] = 'font-weight:bold;color:red';
    $wpmemory_memory['msg_type'] = 'ok';
    return $wpmemory_memory;
}
function wp_memory_admin_page()
{
            require_once WPMEMORYPATH . "/dashboard/dashboard_container.php";
}
function wp_memory_plugin_settings_link($links)
{
    $settings_link = '<a href="admin.php?page=wp_memory_admin_page">Settings</a>';
    array_unshift($links, $settings_link);
    return $links;
}

function wpmemory_add_memory_test($tests)
{
    $tests['direct']['wpmemory_plugin'] = array(
        'label' => __('WP Memory Test', 'wp-memory' ),
        'test' => 'wpmemory_memory_test'
    );
    return $tests;
}
function wpmemory_check($code)
{
  $code = trim($code);
  if(empty($code))
    return false;
  $code = stripNonAlphaNumeric($code);
  $size = strlen($code);
  if (($size != 17) and ($size != 6)  and ($size != 7)  and ($size != 8))
    return false;
  if ($size == 6 or $size == 7 or $size == 8) {
    if (!is_numeric($code))
      return false;
    if ($code < 290000)
      return false;
  }
  /*
  if (($size == 17)) {
    if (is_numeric($code))
      return false;
    if (!ctype_alnum($code))
      return false;
    if (!preg_match('#[0-9]#', $code)) {
      return false;
    }
    if ($code != strtoupper($code))
      return false;
  }
  */
  return true;
}
function wpmemory_updated_message()
{
    echo '<div class="notice notice-success is-dismissible">';
    echo '<br /><b>';
    echo esc_attr__('Database Updated!', 'wp-memory' );
    echo '<br /><br /></div>';
}


function wpmemory_update()
{
    global $wpmemory_checkversion;


    $wpmemory_termina = get_transient('wpmemory_termina');


      //  if (empty($wpmemory_checkversion) or $wpmemory_termina !== false)
        if (empty($wpmemory_checkversion) or  trim($wpmemory_checkversion) == ''  or $wpmemory_termina !== false) {
            return;
        }


    ob_start();
    $domain_name = get_site_url();
    $urlParts = parse_url($domain_name);
    $domain_name = preg_replace('/^www\./', '', $urlParts['host']);
    $myarray = array(
        'domain_name' => $domain_name,
        'wpmemory_checkversion' => $wpmemory_checkversion,
        'wpmemory_version' => WPMEMORYVERSION
    );
    $url = "https://wpmemory.com/api/httpapi.php";
    $response = wp_remote_post($url, array(
        'method' => 'POST',
        'timeout' => 5,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking' => true,
        'headers' => array(),
        'body' => $myarray,
        'cookies' => array()
    ));
    if (is_wp_error($response)) {
        set_transient('wpmemory_termina', DAY_IN_SECONDS, DAY_IN_SECONDS);
        ob_end_clean();
        return;
    }
    $r = trim($response['body']);
    $r = json_decode($r, true);
    $q = count($r);
    if ($q == 1) {
        $botip = trim($r[0]['ip']);
        if ($botip == '-9') {
            update_option('wpmemory_checkversion', '');
        }
        else
           set_transient('wpmemory_termina', DAY_IN_SECONDS, (30 * DAY_IN_SECONDS)  );
    }
    else
    {
        set_transient('wpmemory_termina', DAY_IN_SECONDS, DAY_IN_SECONDS);
    }
    ob_end_clean();
}

function wpmemory_errors()
{
	//wpmemory_options21
	if (isset($_GET['page'])) 
		$page = sanitize_text_field($_GET['page']);
    else
       $page = '';
		if ($page !== 'wp_memory_admin_page')
			return;


	$wpmemory_count = 0;
	define('WPMEMORYPLUGINPATH', plugin_dir_path(__file__));
	$wpmemory_themePath = get_theme_root();
	$error_log_path = trim(ini_get('error_log'));
	if (!is_null($error_log_path) and $error_log_path != trim(ABSPATH . "error_log")) {
		$wpmemory_folders = array(
			$error_log_path,
			ABSPATH . "error_log",
			ABSPATH . "php_errorlog",
			WPMEMORYPLUGINPATH . "/error_log",
			WPMEMORYPLUGINPATH . "/php_errorlog",
			$wpmemory_themePath . "/error_log",
			$wpmemory_themePath . "/php_errorlog"
		);
	} else {
		$wpmemory_folders = array(
			ABSPATH . "error_log",
			ABSPATH . "php_errorlog",
			WPMEMORYPLUGINPATH . "/error_log",
			WPMEMORYPLUGINPATH . "/php_errorlog",
			$wpmemory_themePath . "/error_log",
			$wpmemory_themePath . "/php_errorlog"
		);
	}
	$wpmemory_admin_path = str_replace(get_bloginfo('url') . '/', ABSPATH, get_admin_url());
	array_push($wpmemory_folders, $wpmemory_admin_path . "/error_log");
	array_push($wpmemory_folders, $wpmemory_admin_path . "/php_errorlog");
	$wpmemory_plugins = array_slice(scandir(WPMEMORYPLUGINPATH), 2);
	foreach ($wpmemory_plugins as $wpmemory_plugin) {
		if (is_dir(WPMEMORYPLUGINPATH . "/" . $wpmemory_plugin)) {
			array_push($wpmemory_folders, WPMEMORYPLUGINPATH . "/" . $wpmemory_plugin . "/error_log");
			array_push($wpmemory_folders, WPMEMORYPLUGINPATH . "/" . $wpmemory_plugin . "/php_errorlog");
		}
	}
	$wpmemory_themes = array_slice(scandir($wpmemory_themePath), 2);
	foreach ($wpmemory_themes as $wpmemory_theme) {
		if (is_dir($wpmemory_themePath . "/" . $wpmemory_theme)) {
			array_push($wpmemory_folders, $wpmemory_themePath . "/" . $wpmemory_theme . "/error_log");
			array_push($wpmemory_folders, $wpmemory_themePath . "/" . $wpmemory_theme . "/php_errorlog");
		}
	}
	// echo WPMEMORYURL.'images/logo.png';

	echo '<center>';
	echo '<h2>';
	echo esc_attr__("The lasts lines of the log files.", "wp-memory");
	echo '</h2>';
	

	echo '<h4>';
	echo esc_attr__("For bigger files, download and open them in your local computer.", "wp-memory");
	
	echo '<br />';
    echo '<a href="https://wptoolsplugin.com/site-language-error-can-crash-your-site/">';
	echo esc_attr__("Click to Learn More About Server Errors.", "wp-memory");
	echo '</a>';
	
	echo '<br />';


	echo '</h4>';
    echo '</center>';


	foreach ($wpmemory_folders as $wpmemory_folder) {
		foreach (glob($wpmemory_folder) as $wpmemory_filename) {
			if (strpos($wpmemory_filename, 'backup') != true) {
				echo '<hr>';
				echo '<strong>';
				echo esc_attr(wpmemory_sizeFilter(filesize($wpmemory_filename)));
				echo ' - ';
				echo esc_attr($wpmemory_filename);
				echo '</strong>';
				$wpmemory_count++;
				$marray = wpmemory_read_file($wpmemory_filename, 1000);

				if (gettype($marray) != 'array' or count($marray) < 1) {
					continue;
				}



				$total = count($marray);


				if (count($marray) > 0) {
					echo '<textarea style="width:100%" id="anti_hacker" rows="12">';
					
					

					if($total > 200)
					  $total = 200;
					
					for ($i = 0; $i < $total; $i++) {

						$pos = strpos($marray[$i] , ']' );

						if ($pos !== false) {

							// Data
							
							echo 'Date: '.esc_attr(substr($marray[$i],0,$pos+1));
							$marray[$i] = substr($marray[$i],$pos+1);
							echo PHP_EOL;
							


							$pos = strpos($marray[$i] , ':' );
							if ($pos !== false) {



								
								echo 'Type: '.esc_attr(substr($marray[$i],0,$pos));
								$marray[$i] = substr($marray[$i],$pos+1);
								echo PHP_EOL;
								


								// $pos = strpos($marray[$i] , 'on line' );
								$pos = strpos($marray[$i] , 'in /' );

								//$pos = strpos($marray[$i] , 'in /' );

									if ($pos !== false) {
										

										echo 'Message: '.trim(esc_attr(substr($marray[$i], 0, $pos)));
										echo PHP_EOL;
										echo 'In: '.trim(esc_attr(substr($marray[$i], $pos +2 )));
										echo PHP_EOL;
										$marray[$i] = substr($marray[$i],$pos+1);


									}
									else
									{ // in /
										echo 'Message: '.trim(esc_attr($marray[$i]));
										echo PHP_EOL;

									}
									
									


									// on line
									//echo trim(esc_attr(substr($marray[$i], $pos)));
									//echo PHP_EOL;
								//}
								/*
								else
								{
								   echo trim(esc_attr(substr($marray[$i], $pos + 1)));
								   echo PHP_EOL;						
								}
								*/


							}
							else { // :
							   echo esc_attr($marray[$i]);
							}
							   


						} 
						else  { // ']' 
					    	echo esc_attr($marray[$i]);
						}
						  



						echo PHP_EOL;


					} // end for...

					echo '</textarea>';
				}
				echo '<br />';
			}
		}
	}
	echo "<p>" . esc_attr(__("Log Files found", "wp-memory")) . ": " . esc_attr($wpmemory_count) . "</p>";
}

function wpmemory_sizeFilter($bytes)
{
	$label = array('Bytes', 'KB', 'MB', 'GB', 'TB', 'PB');
	for ($i = 0; $bytes >= 1024 && $i < (count($label) - 1); $bytes /= 1024, $i++);
	return (round($bytes, 2) . " " . $label[$i]);
}

function wpmemory_read_file($file, $lines)
{
	try {
		$handle = fopen($file, "r");
	} catch (Exception $e) {
		return '';
	}
	if (!$handle)
		return '';

	$linecounter = $lines+50;
	$pos = -2;
	$beginning = false;
	$text = array();

	
	while ($linecounter > 0) {
		$t = " ";
		while ($t != "\n") {
			if (fseek($handle, $pos, SEEK_END) == -1) {
				$beginning = true;
				break;
			}
			$t = fgetc($handle);
			$pos--;
		}

		$linecounter--;

		if ($beginning)
			rewind($handle);
			
		$text[$lines - $linecounter - 1] = fgets($handle);
		if ($beginning)
			break;

		
	}

	fclose($handle);


	// remove trace...
	// return array_reverse($text); // array_reverse is optional: you can also just return the $text array which consists of the file's lines.
	
	$text = array_reverse($text);	

	$total = count($text);

	//var_dump($total);




	$text2 = array();
	for($i=0; $i < $total-1; $i++)
	{


		if (preg_match('/stack trace:$/i', $text[$i])) {

			//var_dump($text[$i]);

			//var_dump($text[$i-1]);

			if($i < $total-1)
			   $i++;
			else
			   break;

			/*
			var_dump(preg_match('!^\[(?P<time>[^\]]*)\] PHP\s+(?P<msg>\d+\. .*)$!', $text[$i], $parts));
			var_dump($parts);


			preg_match('!^(?P<msg>#\d+ .*)$!', $text[$i], $parts);
			var_dump($parts);
			*/

			//die();



			$stackTrace = $parts = [];
			//$log->next();

			// $i++;

			if($i < $total-1)
		    	$i++;
		    else
			    break;


			while ((preg_match('!^\[(?P<time>[^\]]*)\] PHP\s+(?P<msg>\d+\. .*)$!', $text[$i], $parts)
				|| preg_match('!^(?P<msg>#\d+ .*)$!', $text[$i], $parts)
				&&  $i < $total)
			) {
	
				$stackTrace[] = $parts['msg'];
				//var_dump($parts);
				//die();
				//$log->next();

				if($i < $total-1)
		        	$i++;
   		        else
			        break;
			}

			if (isset($stackTrace[0]) and substr($stackTrace[0], 0, 2) == '#0') {
				$stackTrace[] = $text[$i];
				// $log->next();

				if($i < $total-1)
				   $i++;
			     else
				   break;
			}
			// $prevError->trace = join("\n", $stackTrace);
		}
         
		/*
		if(isset($stackTrace)) {
		var_dump($stackTrace);
		die();
		}
		*/

		   $text2[] = $text[$i];




	}
	

	return array_reverse($text2);

	// return ($text2);
}
function wpmemory_errors_today($onlytoday)
{
	$wpmemory_count = 0;



	//define('WPMEMORYPATH', plugin_dir_path(__file__));
	//WPMEMORYPATH
	$wpmemory_themePath = get_theme_root();
	$error_log_path = trim(ini_get('error_log'));
	if (!is_null($error_log_path) and $error_log_path != trim(ABSPATH . "error_log")) {
		$wpmemory_folders = array(
			$error_log_path,
			ABSPATH . "error_log",
			ABSPATH . "php_errorlog",
			WPMEMORYPATH . "/error_log",
			WPMEMORYPATH . "/php_errorlog",
			$wpmemory_themePath . "/error_log",
			$wpmemory_themePath . "/php_errorlog"
		);
	} else {
		$wpmemory_folders = array(
			ABSPATH . "error_log",
			ABSPATH . "php_errorlog",
			WPMEMORYPATH . "/error_log",
			WPMEMORYPATH . "/php_errorlog",
			$wpmemory_themePath . "/error_log",
			$wpmemory_themePath . "/php_errorlog"
		);
	}
	$wpmemory_admin_path = str_replace(get_bloginfo('url') . '/', ABSPATH, get_admin_url());
	array_push($wpmemory_folders, $wpmemory_admin_path . "/error_log");
	array_push($wpmemory_folders, $wpmemory_admin_path . "/php_errorlog");
	$wpmemory_plugins = array_slice(scandir(WPMEMORYPATH), 2);
	foreach ($wpmemory_plugins as $wpmemory_plugin) {
		if (is_dir(WPMEMORYPATH . "/" . $wpmemory_plugin)) {
			array_push($wpmemory_folders, WPMEMORYPATH . "/" . $wpmemory_plugin . "/error_log");
			array_push($wpmemory_folders, WPMEMORYPATH . "/" . $wpmemory_plugin . "/php_errorlog");
		}
	}
	$wpmemory_themes = array_slice(scandir($wpmemory_themePath), 2);
	foreach ($wpmemory_themes as $wpmemory_theme) {
		if (is_dir($wpmemory_themePath . "/" . $wpmemory_theme)) {
			array_push($wpmemory_folders, $wpmemory_themePath . "/" . $wpmemory_theme . "/error_log");
			array_push($wpmemory_folders, $wpmemory_themePath . "/" . $wpmemory_theme . "/php_errorlog");
		}
	}



	foreach ($wpmemory_folders as $wpmemory_folder) {


		//// if (gettype($wpmemory_folder) != 'array')
		//	continue;

		if(trim(empty($wpmemory_folder)))
			continue;



		foreach (glob($wpmemory_folder) as $wpmemory_filename) {
			if (strpos($wpmemory_filename, 'backup') != true) {
				$wpmemory_count++;
				$marray = wpmemory_read_file($wpmemory_filename, 20);


				if (gettype($marray) != 'array' or count($marray) < 1) {
					continue;
				}


				if (count($marray) > 0) {
					for ($i = 0; $i < count($marray); $i++) {
						// [05-Aug-2021 08:31:45 UTC]

						if (substr($marray[$i], 0, 1) != '[' or empty($marray[$i]))
							continue;
						$pos = strpos($marray[$i], ' ');
						$string = trim(substr($marray[$i], 1, $pos));
						if (empty($string))
							continue;
						// $data_array = explode('-',$string,);
						$last_date = strtotime($string);
						// var_dump($last_date);
                        
						if($onlytoday == 1) {
							if ((time() - $last_date) < 60 * 60 * 24)
							return true;
						}
						else {
							return true;	
						}
					}
				}
			}
		}
	}
	return false;
}